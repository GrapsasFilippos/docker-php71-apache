DIR := ${CURDIR}

-include $(DIR)/config.mk


PHP_VERSION := $(or $(PHP_VERSION),apache)

MOUNT_STORAGE := /mnt/host-storage

PORT_HTTP := $(or $(PORT_HTTP),8081)
PORT_SSHD := $(or $(PORT_SSHD),2221)
RECIPE := $(or $(RECIPE),default)
WWW_PASS := $(or $(WWW_PASS),secret)

DI_NAME := $(or $(DI_NAME),graphi/php-latest-apache)
DI_TAG := $(or $(DI_TAG),0.5.0)
DC_NAME := $(or $(DC_NAME),php-latest-apache)

DI_ID := $(shell docker images | grep "$(DI_NAME)\s*$(DI_TAG)" | sed -n "s/[0-9a-z\/-]*\s*[0-9\.]*\s*\([0-9a-z]*\).*/\1/p")
DC_ID := $(shell docker ps -a | grep "$(DI_NAME):$(DI_TAG).*$(DC_NAME)" | sed -n "s/\([0-9a-z]*\).*/\1/p")
DC_STARTED_ID := $(shell docker ps | grep "$(DI_NAME):$(DI_TAG).*$(DC_NAME)" | sed -n "s/\([0-9a-z]*\).*/\1/p")


ifeq ($(RECIPE),symfony)
MOUNT := /var/www/symfony/
else ifeq ($(RECIPE),wordpress)
MOUNT := /var/www/wordpress/
else ifeq ($(RECIPE),www)
MOUNT := /var/www/
else
MOUNT := /var/www/html/
endif

ifdef HOST_STORAGE
HOST_STORAGE := -v $(HOST_STORAGE):$(MOUNT_STORAGE)
endif
ifdef HOST_MOUNT
HOST_MOUNT := -v $(HOST_MOUNT):$(MOUNT)
endif
ifdef HOST_MOUNT_SITE_AVAILABLE
HOST_MOUNT_SITE_AVAILABLE := -v $(HOST_MOUNT_SITE_AVAILABLE):/etc/apache2/sites-available/
endif

ifdef LINK_DB_NAME
LINK_DB_NAME := --link $(LINK_DB_NAME):db
endif
ifdef LINK_DB_NAME1
LINK_DB_NAME1 := --link $(LINK_DB_NAME1):db1
endif
ifdef LINK_DB_NAME2
LINK_DB_NAME2 := --link $(LINK_DB_NAME2):db2
endif
ifdef LINK_DB_NAME3
LINK_DB_NAME3 := --link $(LINK_DB_NAME3):db3
endif


docker-bash: docker-start
		$(eval DC_ID := $(shell docker ps -a | grep "$(DI_NAME):$(DI_TAG)" | sed -n "s/\([0-9a-z]*\).*/\1/p"))
		docker exec -it $(DC_ID) /bin/bash

docker-build:
ifeq ($(DI_ID),)
		docker build \
			--build-arg PHP_VERSION=$(PHP_VERSION) \
			--build-arg RECIPE=$(RECIPE) \
			--build-arg WWW_PASS=$(WWW_PASS) \
			-t $(DI_NAME):$(DI_TAG) \
			./Docker
endif

docker-run: docker-build
ifeq ($(DC_ID),)
		docker run \
			$(HOST_STORAGE) \
			$(HOST_MOUNT) \
			$(HOST_MOUNT_SITE_AVAILABLE) \
			-d \
			$(LINK_DB_NAME) \
			$(LINK_DB_NAME1) \
			$(LINK_DB_NAME2) \
			$(LINK_DB_NAME3) \
			-p $(PORT_HTTP):80 \
			-p $(PORT_SSHD):22 \
			--name $(DC_NAME) $(DI_NAME):$(DI_TAG)
endif

docker-start: docker-run
ifeq ($(DC_STARTED_ID),)
		docker start $(DC_NAME)
endif

docker-stop:
		docker stop $(DC_NAME)


docker-clean: docker-clean-container
		docker rmi $(DI_NAME):$(DI_TAG)

docker-clean-container:
		-docker stop $(DC_NAME)
		-docker rm $(DC_NAME)

ssh-login:
		ssh www-data@localhost -p $(PORT_SSHD)

mount-sshfs:
		sshfs www-data@localhost:$(MOUNT) $(SSH_FS) -p $(PORT_SSHD) -o allow_other
